import scala.sys.process._

val Http4sVersion = "0.18.19"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"

val HotSpotJVM = "openjdk:8-jre-alpine"
val OpenJ9JVM = "adoptopenjdk/openjdk8-openj9:slim"

val HotSpotJavaOpts = Seq(
  "-XX:+UnlockExperimentalVMOptions",
  "-XX:+UseCGroupMemoryLimitForHeap",
  "-XX:MaxRAMFraction=1"
)

val OpenJ9JavaOpts = Seq(

)

lazy val nativeImage = taskKey[File]("Build native image")
lazy val nativeImageArtifact = "whisky-api-scala-assembly-1.0-SNAPSHOT"

nativeImage := {
  val assemblyJar = assembly.value

  Process(
    "native-image" ::
    "--no-server" ::
    "--static" ::
      "--enable-http" ::
      "--allow-incomplete-classpath" ::
      "--report-unsupported-elements-at-runtime" ::
      "-H:ReflectionConfigurationFiles=reflection.json" ::
      "--verbose"::
      "-jar" ::
    assemblyJar.getAbsolutePath
      :: Nil) !

    new File(nativeImageArtifact)
}

lazy val whisky = (project in file("."))
  .settings(
    organization := "com.virtuslab",
    name := "whisky-api-scala",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.12.7",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-jetty"        % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "org.specs2"      %% "specs2-core"         % Specs2Version % "test",
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,
      "io.scalaland"    %% "chimney"             % "0.3.0",
      "io.circe"        %% "circe-generic"       % "0.10.0",
      "org.tpolecat"    %% "doobie-core"         % "0.5.3",
      "org.tpolecat"    %% "doobie-hikari"       % "0.5.3",          // HikariCP transactor.
      "org.tpolecat"    %% "doobie-postgres"     % "0.5.3",          // Postgres driver 42.2.5 + type mappings.
    ),
    addCompilerPlugin("org.spire-math" %% "kind-projector"     % "0.9.6"),
    addCompilerPlugin("com.olegpy"     %% "better-monadic-for" % "0.2.4"),
    mainClass in assembly := Some("com.virtuslab.WhiskyMain"),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    dockerfile in docker := {
      val binary: File = nativeImage.value

      val entryPointSeq = Seq("/whisky-api-scala-assembly-1.0-SNAPSHOT")

      new Dockerfile {
        from("alpine:latest")
        add(binary, "/")
        entryPoint(entryPointSeq:_*)
      }
    }
  )
  .enablePlugins(DockerPlugin)

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings",
)
