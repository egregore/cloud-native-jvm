package com.virtuslab

import io.circe.generic.auto._
import io.circe.syntax._
import cats.effect.IO
import org.http4s.{EntityDecoder, HttpService}
import org.http4s.dsl.Http4sDsl
import io.scalaland.chimney.dsl._
import org.http4s.circe._

object WhiskyController extends Http4sDsl[IO] {

  implicit val newWhisky: EntityDecoder[IO, NewWhisky] = jsonOf[IO, NewWhisky]

  def routes(repo: WhiskyRepo): HttpService[IO] = HttpService[IO] {

    case GET -> Root / "whisky" => repo.list.flatMap(list => Ok(list.asJson))

    case GET -> Root / "whisky" / IntVar(id) =>
      for {
        maybeWhisky <- repo.read(id)
        res         <- maybeWhisky.fold(NotFound())(whisky => Ok(whisky.asJson))
      } yield res

    case req @ POST -> Root / "whisky" =>
      for {
        newWhisky <- req.as[NewWhisky]
        id        <- repo.create(newWhisky)
        res       <- Ok(newWhisky.into[Whisky].withFieldConst(_.id, id).transform.asJson)
      } yield res

    case req @ PUT -> Root / "whisky" / IntVar(id) =>
      for {
        newWhisky <- req.as[NewWhisky]
        _         <- repo.update(newWhisky.into[Whisky].withFieldConst(_.id, id).transform)
        res       <- Ok()
      } yield res

    case DELETE -> Root / "whisky" / IntVar(id) =>
      for {
        _   <- repo.delete(id)
        res <- Ok()
      } yield res

  }

}
