package com.virtuslab

import java.util.concurrent.Executors

import cats.effect._
import fs2.{Stream, StreamApp}
import org.http4s.dsl.Http4sDsl
import org.http4s.server.jetty.JettyBuilder
import scala.concurrent.ExecutionContext

case class NewWhisky(name: String, age: Short, region: String, rating: Double)

case class Whisky(id: Int, name: String, age: Short, region: String, rating: Double)

object WhiskyMain extends StreamApp[IO] with Http4sDsl[IO] {

  implicit val ec: ExecutionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, StreamApp.ExitCode] =
    for {
      repo     <- Stream.eval(WhiskyRepo.make)
      exitCode <- server(repo)
    } yield exitCode

  def server(repo: WhiskyRepo): Stream[IO, StreamApp.ExitCode] =
    JettyBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .mountService(WhiskyController.routes(repo), "/")
      .serve

}
