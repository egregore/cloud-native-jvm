package com.virtuslab

import cats.effect.IO
import cats.syntax.functor._

/**
  * DDL:
  * CREATE TABLE whiskies (id SERIAL PRIMARY KEY, name VARCHAR(64), age SMALLINT, region VARCHAR(64), rating REAL);
  */
trait WhiskyRepo {
  def list: IO[List[Whisky]]

  def create(whisky: NewWhisky): IO[Int]

  def read(id: Int): IO[Option[Whisky]]

  def update(whisky: Whisky): IO[Unit]

  def delete(id: Int): IO[Unit]
}

object WhiskyRepo {

  import doobie._
  import doobie.implicits._

  def make: IO[WhiskyRepo] = IO {
    new WhiskyRepo {
      private val xa = Transactor.fromDriverManager[IO](
        "org.postgresql.Driver",
        "jdbc:postgresql://172.18.0.2:5432/whiskydb",
        "drinker",
        ""
      )

      private val listQuery = sql"SELECT id, name, age, region, rating FROM whiskies".query[Whisky].to[List]

      private def fetchQuery(id: Int) =
        sql"SELECT id, name, age, region, rating FROM whiskies WHERE id = $id".query[Whisky].option

      private def insertQuery(newWhisky: NewWhisky) =
        sql"""INSERT INTO whiskies (name, age, region, rating)
             |VALUES(${newWhisky.name}, ${newWhisky.age}, ${newWhisky.region}, ${newWhisky.rating})""".stripMargin.update
          .withUniqueGeneratedKeys[Int]("id")

      private def updateQuery(whisky: Whisky) =
        sql"""UPDATE whiskies
             |  SET name   = ${whisky.name},
             |      age    = ${whisky.age},
             |      region = ${whisky.region},
             |      rating = ${whisky.rating}
             |  WHERE id = ${whisky.id}""".stripMargin.update.run.as(())

      private def deleteQuery(id: Int) =
        sql"DELETE FROM whiskies WHERE id = $id".update.run.as(())

      def list: IO[List[Whisky]] = listQuery transact xa

      def create(whisky: NewWhisky): IO[Int] = insertQuery(whisky) transact xa

      def read(id: Int): IO[Option[Whisky]] = fetchQuery(id) transact xa

      def update(whisky: Whisky): IO[Unit] = updateQuery(whisky) transact xa

      def delete(id: Int): IO[Unit] = deleteQuery(id) transact xa
    }
  }
}
