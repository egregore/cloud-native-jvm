package com.virtuslab;

import lombok.Value;

@Value(staticConstructor = "of")
class NewWhisky {
    String name;
    short age;
    String region;
    float rating;
}
