package com.virtuslab;

import com.google.gson.Gson;
import spark.ExceptionHandler;
import spark.Request;
import spark.Response;

import java.util.Optional;

import static spark.Spark.*;

class WhiskyController {

    private final WhiskyRepo whiskyRepo = WhiskyRepo.make();
    private final Gson gson = new Gson();

    private WhiskyController() {
        get("/whisky", (req, res) -> whiskyRepo.list(), gson::toJson);

        get("/whisky/:id", (req, res) -> {
                    final Optional<Whisky> maybeWhisky = whiskyRepo.read(Integer.parseInt(req.params(":id")));
                    if (maybeWhisky.isPresent()) {
                        return gson.toJson(maybeWhisky.get());
                    } else {
                        res.status(404);
                        return null;
                    }
                });

        post("/whisky", (req, res) -> {
            final NewWhisky newWhisky = gson.fromJson(req.body(), NewWhisky.class);
            final int id = whiskyRepo.create(newWhisky);

            return Whisky.of(id, newWhisky.getName(), newWhisky.getAge(), newWhisky.getRegion(), newWhisky.getRating());
        }, gson::toJson);

        put("/whisky/:id", (req, res) -> {
            final NewWhisky whiskyBody = gson.fromJson(req.body(), NewWhisky.class);
            Whisky updateWhisky = Whisky.of(
                    Integer.parseInt(req.params(":id")),
                    whiskyBody.getName(),
                    whiskyBody.getAge(),
                    whiskyBody.getRegion(),
                    whiskyBody.getRating()
            );

            whiskyRepo.update(updateWhisky);

            return updateWhisky;
        }, gson::toJson);

        delete("/whisky/:id", (req, res) -> {
            whiskyRepo.delete(Integer.parseInt(req.params(":id")));
            return null;
        });

        notFound((req, res) -> {
            res.status(404);
            return null;
        });

        after((req, res) -> res.type("application/json"));

        exception(Exception.class, (ex, req, res) -> {
            ex.printStackTrace();
            res.status(500);
        });

        internalServerError((req, res) -> null);
    }

    void unmount() {
        stop();
    }

    static WhiskyController mount() {
        return new WhiskyController();
    }

}
