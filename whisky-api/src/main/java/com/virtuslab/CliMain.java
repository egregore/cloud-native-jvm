package com.virtuslab;

public class CliMain {
    public static void main(String[] args) {
        final WhiskyRepo whiskyRepo = WhiskyRepo.make();

        //whiskyRepo.create(NewWhisky.of("Dalwhinnie", (short)15, "Highlands", 10f));
        System.out.println(whiskyRepo.read(1).toString());
        whiskyRepo.update(Whisky.of(1, "Dalwhinnie", (short)18, "Skye", 11.2f));
        System.out.println(whiskyRepo.read(1).toString());
        whiskyRepo.delete(1);
    }
}
