package com.virtuslab;

import lombok.Value;

@Value(staticConstructor = "of")
class Whisky {
    int id;
    String name;
    short age;
    String region;
    float rating;
}
