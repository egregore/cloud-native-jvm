package com.virtuslab;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;
import java.util.Optional;

/**
 * DDL:
 * CREATE TABLE whiskies (id SERIAL PRIMARY KEY, name VARCHAR(64), age SMALLINT, region VARCHAR(64), rating REAL);
 */
interface WhiskyRepo {

    List<Whisky> list();

    int create(NewWhisky whisky);

    Optional<Whisky> read(int id);

    void update(Whisky whisky);

    void delete(int id);

    static WhiskyRepo make() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://postgres:5432/whiskydb");
        config.setUsername("drinker");
        config.setPassword("");

        HikariDataSource ds = new HikariDataSource(config);

        return new WhiskyRepo() {

            private final Sql2o sql2o = new Sql2o(ds);

            private final String listSql = "SELECT id, name, age, region, rating FROM whiskies";

            private final String insertSql =
                    "INSERT INTO whiskies(name, age, region, rating) " +
                            "VALUES (:nameParam, :ageParam, :regionParam, :ratingParam)";

            private final String fetchSql =
                    "SELECT id, name, age, region, rating " +
                            "FROM whiskies " +
                            "WHERE id = :idParam";

            private final String updateSql =
                    "UPDATE whiskies " +
                            "SET name = :nameParam, " +
                            "age = :ageParam, " +
                            "region = :regionParam, " +
                            "rating = :ratingParam " +
                            "WHERE id = :idParam";

            private final String deleteSql = "DELETE FROM whiskies WHERE id = :idParam";

            @Override
            public List<Whisky> list() {
                try (Connection con = sql2o.open()) {
                    return con.createQuery(listSql)
                            .executeAndFetch(Whisky.class);
                }
            }

            @Override
            public int create(NewWhisky newWhisky) {
                try (Connection con = sql2o.open()) {
                    return con.createQuery(insertSql)
                            .addParameter("nameParam", newWhisky.getName())
                            .addParameter("ageParam", newWhisky.getAge())
                            .addParameter("regionParam", newWhisky.getRegion())
                            .addParameter("ratingParam", newWhisky.getRating())
                            .executeUpdate().getKey(Integer.class);
                }
            }

            @Override
            public Optional<Whisky> read(int id) {
                try (Connection con = sql2o.open()) {
                    return Optional.ofNullable(
                            con.createQuery(fetchSql)
                                    .addParameter("idParam", id)
                                    .executeAndFetchFirst(Whisky.class)
                    );
                }
            }

            @Override
            public void update(Whisky whisky) {
                try (Connection con = sql2o.open()) {
                    con.createQuery(updateSql)
                            .addParameter("nameParam", whisky.getName())
                            .addParameter("ageParam", whisky.getAge())
                            .addParameter("regionParam", whisky.getRegion())
                            .addParameter("ratingParam", whisky.getRating())
                            .addParameter("idParam", whisky.getId())
                            .executeUpdate();
                }
            }

            @Override
            public void delete(int id) {
                try (Connection con = sql2o.open()) {
                    con.createQuery(deleteSql)
                            .addParameter("idParam", id)
                            .executeUpdate();
                }
            }
        };
    }

}
